﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(PAM_Screenplay))]
public class PAM_ScreenplayEditor : Editor
{
    PAM_Screenplay myTarget;

    Vector2 scrollPos;

    SpeachAction selectedSpeachAction;

    public void OnEnable()
    {
        hideFlags = HideFlags.HideAndDontSave;
        myTarget = (PAM_Screenplay)target;
    }

    public override void OnInspectorGUI()
    {
        GUILayout.Label("Screenplay data", EditorStyles.boldLabel);

        myTarget.scenario.background = (Sprite)EditorGUILayout.ObjectField("Scenario", myTarget.scenario.background, typeof(Sprite), true);

        GUILayout.Label("Actors speachs", EditorStyles.boldLabel);

        DrawActors();

        EditorGUILayout.Space();

        if (GUILayout.Button("New Speach"))
        {
            PAM_ActorSpeach actorSpeach = new PAM_ActorSpeach();
            myTarget.actorsSpeachs.Add(actorSpeach);
        }

        EditorUtility.SetDirty(myTarget);
    }
    
    private void DrawActors()
    {
        foreach (PAM_ActorSpeach actorSpeach in myTarget.actorsSpeachs)
        {
            EditorGUILayout.BeginHorizontal();

            if (actorSpeach.actor == null)
            {
                GUILayout.Label("Actor slot");
                actorSpeach.actor = (PAM_Actor)EditorGUILayout.ObjectField(actorSpeach.actor, typeof(PAM_Actor), false);
            }
            else
            {
                GUILayout.Label(actorSpeach.actor.actorName);
                actorSpeach.actor = (PAM_Actor)EditorGUILayout.ObjectField(actorSpeach.actor, typeof(PAM_Actor), false);
            }

            if (GUILayout.Button("Remove Speach"))
            {
                myTarget.actorsSpeachs.Remove(actorSpeach);
                return;
            }

            EditorGUILayout.EndHorizontal();

            DrawActorsActions(actorSpeach);

            EditorGUILayout.BeginHorizontal();

            selectedSpeachAction = (SpeachAction)EditorGUILayout.EnumPopup(selectedSpeachAction);

            if (GUILayout.Button("New action"))
            {
                actorSpeach.actions.Add(new PAM_SpeachActionData(selectedSpeachAction));
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();
        }

        if (GUILayout.Button("Reset"))
        {
            foreach (PAM_ActorSpeach actorSpeach in myTarget.actorsSpeachs)
            {
                foreach (PAM_SpeachActionData data in actorSpeach.actions)
                {
                    data.isCompleted = false;
                }
            }
        }
    }

    private void DrawActorsActions(PAM_ActorSpeach actorSpeach)
    {
       // scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.ExpandWidth(true));

        foreach (PAM_SpeachActionData action in actorSpeach.actions)
        {
            EditorGUILayout.BeginHorizontal();

            switch (action.Type)
            {
                case SpeachAction.PARENTHETICAL:
                    DrawActionParenthetical(action);
                    break;
                case SpeachAction.DIALOGUE:
                    DrawActionDialogue(action, actorSpeach.actor);
                    break;
                case SpeachAction.OPTIONS:
                    DrawActionOptions(action);
                    break;
                case SpeachAction.SFX:
                    DrawActionSFX(action);
                    break;
                case SpeachAction.MUSIC:
                    DrawActionMusic(action);
                    break;
                case SpeachAction.BACKGROUND:
                    DrawActionBackground(action);
                    break;
                case SpeachAction.WAITFORSECONDS:
                    DrawActionWait(action);
                    break;
                case SpeachAction.OPEN_SCREENPLAY:
                    DrawActionOpenScreenplay(action);
                    break;
                case SpeachAction.LOAD_SCENE:
                    DrawActionLoadScene(action);
                    break;
                case SpeachAction.INIT_GAME:
                    DrawActionInitGame(action);
                    break;
            }

            if (GUILayout.Button("-", GUILayout.Width(50)))
            {
                actorSpeach.actions.Remove(action);
                return;
            }

            EditorGUILayout.EndHorizontal();
        }

        //GUILayout.EndScrollView();
    }

    private void DrawActionInitGame(PAM_SpeachActionData data)
    {
        EditorGUILayout.LabelField("Init game", EditorStyles.boldLabel);
    }

    private void DrawActionOpenScreenplay(PAM_SpeachActionData data)
    {
        EditorGUILayout.BeginVertical();

        data.nextScreenplay = (PAM_Screenplay)EditorGUILayout.ObjectField("Next Screenplay", data.nextScreenplay, typeof(PAM_Screenplay), true);

        EditorGUILayout.LabelField("Start at");

        if (data.nextScreenplay != null)
            data.initLine = EditorGUILayout.IntSlider(data.initLine, 0, data.nextScreenplay.actorsSpeachs.Count - 1);

        EditorGUILayout.EndVertical();
    }

    private void DrawActionLoadScene(PAM_SpeachActionData data)
    {
        EditorGUILayout.BeginVertical();

        List<string> scenes = new List<string>();

        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if (scene.enabled)
            {
                string path = scene.path;
                int pos = path.LastIndexOf("/") + 1;

                string sceneName = path.Substring(pos, path.Length - pos);

                sceneName = sceneName.Split('.')[0]; 

                scenes.Add(sceneName);
            }
        }

        data.nextScene = scenes[EditorGUILayout.Popup(scenes.IndexOf(data.nextScene), scenes.ToArray())];

        EditorGUILayout.EndVertical();
    }

    private void DrawActionParenthetical(PAM_SpeachActionData data)
    {
        data.changeExpressionTo = (ActorParenthetical)EditorGUILayout.EnumPopup("Next expression", data.changeExpressionTo);
    }

    private void DrawActionDialogue(PAM_SpeachActionData data, PAM_Actor actor)
    {
        EditorGUILayout.BeginVertical();

        EditorStyles.textField.wordWrap = true;
        data.dialogue = EditorGUILayout.TextArea(data.dialogue, GUILayout.Height(30));

        EditorGUILayout.BeginHorizontal();
        data.font = (Font) EditorGUILayout.ObjectField("Optional Font", data.font, typeof(Font), true);

        if (data.font != null)
        {
            GUIStyle style = new GUIStyle
            {
                font = data.font
            };
            EditorGUILayout.LabelField("Preview", style);
        }

        if (actor != null)
            data.actorName = actor.actorName;

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();
    }

    private void DrawActionOptions(PAM_SpeachActionData data)
    {
        if (GUILayout.Button("Edit actions"))
        {
            PAM_OptionEditor.EditOptions(data);
        }
    }

    private void DrawActionSFX(PAM_SpeachActionData data)
    {
        data.sfx = (AudioClip)EditorGUILayout.ObjectField("SFX", data.sfx, typeof(AudioClip), false);
    }

    private void DrawActionMusic(PAM_SpeachActionData data)
    {
        data.music = (AudioClip)EditorGUILayout.ObjectField("Music", data.music, typeof(AudioClip), false);
    }

    private void DrawActionBackground(PAM_SpeachActionData data)
    {
        data.background = (Sprite)EditorGUILayout.ObjectField("Music", data.background, typeof(Sprite), false);
    }

    private void DrawActionWait(PAM_SpeachActionData data)
    {
        data.delay = EditorGUILayout.FloatField("Wait time", data.delay);
    }
}

public class PAM_ScreenPlayWindow: EditorWindow
{
    string path = "Assets/PAM/Screenplay/Librettos";
    string fileName = "new Screenplay";

    [MenuItem("PAM/Conversation/Create")]
    static void Init()
    {
        PAM_ScreenPlayWindow window = (PAM_ScreenPlayWindow)EditorWindow.GetWindow(typeof(PAM_ScreenPlayWindow));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Creator", EditorStyles.boldLabel);
        path = EditorGUILayout.TextField("Path", path);

        fileName = EditorGUILayout.TextField("Title", fileName);

        if (GUILayout.Button("Create"))
        {
            string finalPath = path + "/" + fileName + ".asset";
            AssetDatabase.CreateAsset(new PAM_Screenplay(), finalPath);
        }
    }
}

public class PAM_ActorWindow : EditorWindow
{
    string path = "Assets/PAM/Screenplay/Actors";
    string actorName;

    Sprite sprite_default;
    Sprite sprite_happy;
    Sprite sprite_angry;
    Sprite sprite_scary;
    Sprite sprite_ashamed;
    Sprite sprite_sad;

    [MenuItem("PAM/Actors/Create")]
    static void Init()
    {
        PAM_ActorWindow window = (PAM_ActorWindow)EditorWindow.GetWindow(typeof(PAM_ActorWindow));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Actor Creator", EditorStyles.boldLabel);
        path = EditorGUILayout.TextField("Path", path);

        actorName = EditorGUILayout.TextField("Actor Name", actorName);

        GUILayout.Label("Expresions");

        sprite_default = (Sprite)EditorGUILayout.ObjectField("Default", sprite_default, typeof(Sprite), true);

        sprite_happy = (Sprite)EditorGUILayout.ObjectField("Happy", sprite_happy, typeof(Sprite), true);

        sprite_angry = (Sprite)EditorGUILayout.ObjectField("Angry", sprite_angry, typeof(Sprite), true);

        sprite_scary = (Sprite)EditorGUILayout.ObjectField("Scary", sprite_scary, typeof(Sprite), true);

        sprite_ashamed = (Sprite)EditorGUILayout.ObjectField("Ashamed", sprite_ashamed, typeof(Sprite), true);

        sprite_sad = (Sprite)EditorGUILayout.ObjectField("Sad", sprite_sad, typeof(Sprite), true);

        if (actorName != null && actorName.Length > 0)
        {
            if (GUILayout.Button("Create"))
            {
                PAM_Actor actor = new PAM_Actor
                {
                    actorName = actorName,
                    sprite_default = sprite_default,
                    sprite_happy = sprite_happy,
                    sprite_angry = sprite_angry,
                    sprite_scary = sprite_scary,
                    sprite_ashamed = sprite_ashamed,
                    sprite_sad = sprite_sad
                };

                AssetDatabase.CreateAsset(actor, path + "/" + actorName + ".asset");

                EditorUtility.SetDirty(actor);
            }
        }
    }
}

public class PAM_OptionEditor : EditorWindow
{
    public PAM_SpeachActionData current;

    public static void EditOptions(PAM_SpeachActionData data)
    {
        PAM_OptionEditor window = (PAM_OptionEditor)EditorWindow.GetWindow(typeof(PAM_OptionEditor));
        window.current = data;
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Speach Options Editor", EditorStyles.boldLabel);

        bool foundError = false;

        foreach (PAM_SpeachActionOption option in current.options)
        {
            EditorGUILayout.BeginHorizontal();

            option.OptionType = (ActionOption)EditorGUILayout.EnumPopup("OptionType", option.OptionType);

            if (GUILayout.Button("∧"))
            {
                int index = current.options.IndexOf(option) - 1;

                if (index < 0)
                    index = current.options.Count - 1;

                PAM_SpeachActionOption aux = option;
                current.options.Remove(option);
                current.options.Insert(index, option);
                return;
            }

            if (GUILayout.Button("∨"))
            {
                int index = current.options.IndexOf(option) + 1;

                if (index > current.options.Count - 1)
                    index = 0;

                PAM_SpeachActionOption aux = option;
                current.options.Remove(option);
                current.options.Insert(index, option);
                return;

            }

            if (GUILayout.Button("x"))
            {
                current.options.Remove(option);
                return;
            }

            EditorGUILayout.EndHorizontal();

            if (option.OptionType.Equals(ActionOption.SELECT_ONE))
                foundError = true;

            EditorGUILayout.LabelField("Option Message");

            option.message = EditorGUILayout.TextField(option.message);

            switch (option.OptionType)
            {
                case ActionOption.CONTINUE_SPEACH:
                    DrawContinueSpeach(option);
                    break;
                case ActionOption.START_SCREENPLAY:
                    DrawStartScreenplay(option);
                    break;
                case ActionOption.CONTINUE_SCREENPLAY:
                    DrawContinueScreenplay(option);
                    break;
            }
        }

        if (GUILayout.Button("Add new option"))
        {
            current.options.Add(new PAM_SpeachActionOption());
        }

        if(foundError)
        {
            EditorGUILayout.Space();

            var style = new GUIStyle(GUI.skin.label);
            style.normal.textColor = Color.red;

            GUILayout.Label("All options must have an option type!", style);
        }
    }

    private void DrawContinueSpeach(PAM_SpeachActionOption option)
    {

    }

    private void DrawStartScreenplay(PAM_SpeachActionOption option)
    {
        option.nextScreenPlay = (PAM_Screenplay)EditorGUILayout.ObjectField("Next Screenplay", option.nextScreenPlay, typeof(PAM_Screenplay), true);
    }

    private void DrawContinueScreenplay(PAM_SpeachActionOption option)
    {
        option.nextScreenPlay = (PAM_Screenplay)EditorGUILayout.ObjectField("Change to Screenplay", option.nextScreenPlay, typeof(PAM_Screenplay), true);

        if (option.nextScreenPlay != null)
        {
            option.initLine = EditorGUILayout.IntSlider("Init line", option.initLine, 0, option.nextScreenPlay.actorsSpeachs.Count - 1);
        }
    }
}