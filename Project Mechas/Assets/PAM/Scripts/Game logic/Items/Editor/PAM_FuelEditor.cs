﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PAM_Fuel))]
public class PAM_FuelEditor : Editor
{
    
}

public class PAM_FuelCreator : EditorWindow
{
    string path = "Assets/PAM/Screenplay/Items/Fuel";

    Sprite icon;
    string itemName;
    PAM_Item.ItemType type;
    PAM_Fuel.FuelBase fuelBase;

    [MenuItem("PAM/Vehicles/Fuel/Create")]
    static void Init()
    {
        PAM_FuelCreator window = (PAM_FuelCreator)EditorWindow.GetWindow(typeof(PAM_FuelCreator));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Fuel Creator", EditorStyles.boldLabel);

        path = EditorGUILayout.TextField(path);

        EditorGUILayout.Space();

        GUILayout.Label("Fuel Name");
        itemName = EditorGUILayout.TextField(itemName);

        GUILayout.Label("Fuel Icon");
        icon = (Sprite)EditorGUILayout.ObjectField(icon, typeof(Sprite), true);

        GUILayout.Label("Item Type");
        type = (PAM_Item.ItemType)EditorGUILayout.EnumPopup(type);

        GUILayout.Label("Fuel Type");
        fuelBase = (PAM_Fuel.FuelBase)EditorGUILayout.EnumPopup(fuelBase);


        if (GUILayout.Button("Create"))
        {
            AssetDatabase.CreateAsset(new PAM_Fuel(icon, itemName, type, fuelBase), path + "/" + itemName + ".asset");
        }
    }
}