﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PAM_Garage))]
public class PAM_GarageEditor : Editor
{
    PAM_Garage myTarget;

    private PAM_Vehicle vehicle;

    public void OnEnable()
    {
        myTarget = (PAM_Garage)target;    
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Total vehicles: " + myTarget.storage.Count);

        EditorGUILayout.Space();

        foreach (PAM_Garage.GarageSlot storage in myTarget.storage)
        {
            EditorGUILayout.LabelField(storage.vehicle.model + " / " + storage.vehicle.GetVehicleType());

            EditorGUILayout.BeginHorizontal();

            Texture2D myTexture = AssetPreview.GetAssetPreview(storage.vehicle.icon);
            GUILayout.Label(myTexture, GUILayout.Width(50), GUILayout.Height(50));

            EditorGUILayout.LabelField("Age");
            storage.age = EditorGUILayout.FloatField(storage.age, GUILayout.Width(50));

            if (storage.vehicle.GetVehicleType().Equals(PAM_Vehicle.VehicleType.CARGO))
            {
                if (GUILayout.Button("See cargo"))
                {
                    CargoInfo.Init(storage);
                }
            }

            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Remove"))
            {
                myTarget.RemoveVehicle(storage);
                vehicle = null;
                return;
            }
        }

        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();

        vehicle = (PAM_Vehicle)EditorGUILayout.ObjectField(vehicle, typeof(PAM_Vehicle), true);

        if (vehicle)
        {
            if (GUILayout.Button("Register"))
            {
                myTarget.AddVehicle(vehicle);
                vehicle = null;
            }
        }

        EditorGUILayout.EndHorizontal();

        EditorUtility.SetDirty(myTarget);
    }


    private class CargoInfo : EditorWindow
    {
        public PAM_Garage.GarageSlot current;
        public PAM_VehicleCargo vehicle;

        public static void Init(PAM_Garage.GarageSlot slot)
        {
            CargoInfo window = (CargoInfo)EditorWindow.GetWindow(typeof(CargoInfo));
            window.current = slot;
            window.vehicle = (PAM_VehicleCargo)slot.vehicle;
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("Cargo of " + current.vehicle.model, EditorStyles.boldLabel);

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Cargo: " + current.inventory.Volume + " / " + vehicle.MaxCapacity);

            foreach (PAM_ItemStorage storage in current.inventory.storage)
            {
                EditorGUILayout.LabelField(storage.item.itemName + " / " + storage.item.type);

                EditorGUILayout.BeginHorizontal();

                Texture2D myTexture = AssetPreview.GetAssetPreview(storage.item.icon);
                GUILayout.Label(myTexture, GUILayout.Width(50), GUILayout.Height(50));

                storage.amount = EditorGUILayout.IntField(storage.amount, GUILayout.Width(50));
                EditorGUILayout.EndHorizontal();
            }
        }
    }
}