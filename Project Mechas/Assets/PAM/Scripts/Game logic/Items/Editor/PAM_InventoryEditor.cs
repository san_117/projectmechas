﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PAM_Inventory))]
public class PAM_InventoryEditor : Editor
{
    PAM_Inventory myTarget;

    private PAM_Item item;

    public void OnEnable()
    {
        myTarget = (PAM_Inventory)target;    
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Total items: " + myTarget.storage.Count);

        EditorGUILayout.Space();

        foreach (PAM_ItemStorage storage in myTarget.storage)
        {
            EditorGUILayout.LabelField(storage.item.itemName + " / " + storage.item.type);

            EditorGUILayout.BeginHorizontal();

            Texture2D myTexture = AssetPreview.GetAssetPreview(storage.item.icon);
            GUILayout.Label(myTexture, GUILayout.Width(50), GUILayout.Height(50));

            storage.amount = EditorGUILayout.IntField(storage.amount, GUILayout.Width(50));
            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();

        item = (PAM_Item)EditorGUILayout.ObjectField(item, typeof(PAM_Item), true);

        if (item)
        {
            if (GUILayout.Button("Register"))
            {
                myTarget.AddItem(item, 0);
                item = null;
            }
        }

        EditorGUILayout.EndHorizontal();

        EditorUtility.SetDirty(myTarget);
    }
}