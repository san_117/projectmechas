﻿using UnityEngine;
using UnityEditor;

public class PAM_ItemCreator : EditorWindow
{
    string path = "Assets/PAM/Screenplay/Items";

    string itemName;
    Sprite icon;
    PAM_Item.ItemType type;

    [MenuItem("PAM/Items/Create")]
    static void Init()
    {
        PAM_ItemCreator window = (PAM_ItemCreator)EditorWindow.GetWindow(typeof(PAM_ItemCreator));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Item Creator", EditorStyles.boldLabel);

        path = EditorGUILayout.TextField(path);

        EditorGUILayout.Space();

        GUILayout.Label("Item Name");
        itemName = EditorGUILayout.TextField(itemName);

        GUILayout.Label("Item Icon");
        icon = (Sprite)EditorGUILayout.ObjectField(icon, typeof(Sprite), true);

        GUILayout.Label("Item Type");
        type = (PAM_Item.ItemType) EditorGUILayout.EnumPopup(type);

        if (GUILayout.Button("Create"))
        {
            AssetDatabase.CreateAsset(new PAM_Item(icon, itemName, type), path + "/" + itemName + ".asset");
        }
    }
}

public class PAM_InventoryCreator : EditorWindow
{
    string path = "Assets/PAM/Screenplay/Inventory";

    [MenuItem("PAM/Inventory/Warehouse/Create")]
    static void Init()
    {
        PAM_InventoryCreator window = (PAM_InventoryCreator)EditorWindow.GetWindow(typeof(PAM_InventoryCreator));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Warehouse Creator", EditorStyles.boldLabel);

        path = EditorGUILayout.TextField(path);

        EditorGUILayout.Space();

        if (GUILayout.Button("Create"))
        {
            AssetDatabase.CreateAsset(new PAM_Inventory(), path + "/Warehouse.asset");
        }
    }
}

public class PAM_GarageCreator : EditorWindow
{
    string path = "Assets/PAM/Screenplay/Garage";

    [MenuItem("PAM/Inventory/Garage/Create")]
    static void Init()
    {
        PAM_GarageCreator window = (PAM_GarageCreator)EditorWindow.GetWindow(typeof(PAM_GarageCreator));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Garage Creator", EditorStyles.boldLabel);

        path = EditorGUILayout.TextField(path);

        EditorGUILayout.Space();

        if (GUILayout.Button("Create"))
        {
            AssetDatabase.CreateAsset(new PAM_Garage(), path + "/Garage.asset");
        }
    }
}

