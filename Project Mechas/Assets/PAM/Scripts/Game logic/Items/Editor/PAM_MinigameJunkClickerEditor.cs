﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PAM_MinigameJunkClicker))]
public class PAM_MinigameJunkClickerEditor : Editor
{
    PAM_MinigameJunkClicker myTarget;
    private int index;

    public void OnEnable()
    {
        myTarget = (PAM_MinigameJunkClicker)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        myTarget.garage = (PAM_Garage)EditorGUILayout.ObjectField(myTarget.garage, typeof(PAM_Garage), true);

        if(myTarget.garage != null)
        {
            index = EditorGUILayout.IntSlider(index, 0, myTarget.garage.storage.Count - 1);

            myTarget.garageSlotIndex = index;
        }

        EditorUtility.SetDirty(myTarget);
    }
}