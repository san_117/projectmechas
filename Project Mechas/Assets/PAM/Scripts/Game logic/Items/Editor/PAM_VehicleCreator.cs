﻿using UnityEngine;
using UnityEditor;

public class PAM_VehicleCreator : EditorWindow
{
    string path = "Assets/PAM/Screenplay/Vehicles";

    string model;
    float value;
    Sprite icon;
    PAM_Fuel fuel;
    PAM_Vehicle.VehicleType vehicleType;
    PAM_VehicleMecha.MechaType mechaType;
    int maxCapacity;

    [MenuItem("PAM/Vehicles/Create")]
    static void Init()
    {
        PAM_VehicleCreator window = (PAM_VehicleCreator)EditorWindow.GetWindow(typeof(PAM_VehicleCreator));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Vehicle Creator", EditorStyles.boldLabel);

        path = EditorGUILayout.TextField(path);

        EditorGUILayout.Space();

        GUILayout.Label("Vehicle Model");
        model = EditorGUILayout.TextField(model);

        GUILayout.Label("Vehicle Value");
        value = EditorGUILayout.FloatField(value);

        GUILayout.Label("Fuel");
        fuel = (PAM_Fuel)EditorGUILayout.ObjectField(fuel, typeof(PAM_Fuel), true);

        GUILayout.Label("Vehicle Icon");
        icon = (Sprite)EditorGUILayout.ObjectField(icon, typeof(Sprite), true);

        GUILayout.Label("Vehicle Type");
        vehicleType = (PAM_Vehicle.VehicleType)EditorGUILayout.EnumPopup(vehicleType);

        switch (vehicleType)
        {
            case PAM_Vehicle.VehicleType.TRANSPORT:
                break;
            case PAM_Vehicle.VehicleType.CARGO:
                GUILayout.Label("Max Capacity");
                maxCapacity = EditorGUILayout.IntField(maxCapacity);
                break;
            case PAM_Vehicle.VehicleType.MECHA:
                GUILayout.Label("Mecha Type");
                mechaType = (PAM_VehicleMecha.MechaType)EditorGUILayout.EnumPopup(mechaType);
                break;
        }

        if (GUILayout.Button("Create"))
        {
            switch (vehicleType)
            {
                case PAM_Vehicle.VehicleType.TRANSPORT:
                    AssetDatabase.CreateAsset(new PAM_VehicleTransport(model, value, icon, fuel, vehicleType), path + "/" + model + "_" + vehicleType + ".asset");
                    break;
                case PAM_Vehicle.VehicleType.CARGO:
                    AssetDatabase.CreateAsset(new PAM_VehicleCargo(model, value, icon, fuel, vehicleType, maxCapacity), path + "/" + model + "_" + vehicleType + ".asset");
                    break;
                case PAM_Vehicle.VehicleType.MECHA:
                    AssetDatabase.CreateAsset(new PAM_VehicleMecha(model, value, icon, fuel, vehicleType, mechaType), path + "/" + model + "_" + vehicleType + ".asset");
                    break;
            }
        }
    }
}