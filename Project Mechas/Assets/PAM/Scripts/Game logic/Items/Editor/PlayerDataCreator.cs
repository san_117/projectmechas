﻿using UnityEngine;
using UnityEditor;

public class PlayerDataCreator : EditorWindow
{
    string path = "Assets/PAM/Screenplay/Player Data";

    [MenuItem("PAM/Data/Player/Create")]
    static void Init()
    {
        PlayerDataCreator window = (PlayerDataCreator)EditorWindow.GetWindow(typeof(PlayerDataCreator));
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Vehicle Creator", EditorStyles.boldLabel);

        path = EditorGUILayout.TextField(path);

        EditorGUILayout.Space();

        if (GUILayout.Button("Create"))
        {
            AssetDatabase.CreateAsset(new PAM_PlayerData(), path + "/PlayerData.asset");
        }
    }
}