﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PAM_Fuel : PAM_Item
{
    public FuelBase fuelBase;

    public PAM_Fuel(Sprite icon, string itemName, ItemType type, FuelBase fuelBase) : base(icon, itemName, type)
    {
        this.icon = icon;
        this.itemName = itemName;
        this.type = type;
        this.fuelBase = fuelBase;
    }

    public enum FuelBase
    {
        FOSSIL, ORGANIC, NUCLEAR
    }
}
