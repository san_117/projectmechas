﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class PAM_Garage : ScriptableObject
{
    public List<GarageSlot> storage = new List<GarageSlot>();

    public void RemoveVehicle(GarageSlot target)
    {
        storage.Remove(target);
    }

    public void AddVehicle(PAM_Vehicle newVehicle)
    {
        storage.Add(new GarageSlot(newVehicle, 0));
    }

    public void AddVehicle(PAM_Vehicle newVehicle, float age)
    {
        storage.Add(new GarageSlot(newVehicle, age));
    }

    [System.Serializable]
    public class GarageSlot
    {
        public PAM_Vehicle vehicle;
        public GarageInventory inventory = new GarageInventory();
        public float age;

        public GarageSlot(PAM_Vehicle vehicle, float age)
        {
            this.vehicle = vehicle;
            this.age = age;
            this.inventory.Init(this);
        }

        public PAM_Vehicle.VehicleType GetVehicleType()
        {
            return vehicle.GetVehicleType();
        }

        public void AddItem(PAM_Item item, int amount)
        {
            inventory.AddItem(item, amount);
        }

        [System.Serializable]
        public class GarageInventory
        {
            [SerializeField]
            private GarageSlot slot;

            public List<PAM_ItemStorage> storage = new List<PAM_ItemStorage>();

            public void Init(GarageSlot slot)
            {
                Debug.Log(slot.vehicle);
                this.slot = slot;
            }

            public override string ToString()
            {
                if(slot.vehicle.GetVehicleType().Equals(PAM_Vehicle.VehicleType.CARGO))
                {
                    PAM_VehicleCargo cast = (PAM_VehicleCargo)slot.vehicle;

                    return Volume + " / " + cast.MaxCapacity;
                }
                else
                {
                    return "This vehicle doesn't have inventory";
                }
            }

            public int Volume
            {
                get
                {
                    int aux = 0;

                    foreach (PAM_ItemStorage item in storage)
                    {
                        aux += item.GetWeight();
                    }

                    return aux;
                }
            }

            public bool IsFull
            {
                get
                {
                    if (slot.GetVehicleType().Equals(PAM_Vehicle.VehicleType.CARGO))
                    {
                        PAM_VehicleCargo cast = (PAM_VehicleCargo)slot.vehicle;
                        return (Volume >= cast.MaxCapacity);
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            public void AddItem(PAM_Item item, int amount)
            {
                foreach (PAM_ItemStorage storedItem in storage)
                {
                    if (storedItem.item.name.Equals(item.name))
                    {
                        storedItem.amount += amount;
                        return;
                    }
                }

                storage.Add(new PAM_ItemStorage(item, amount));
            }

            public void RemoveItem(PAM_ItemStorage storedItem)
            {
                storage.Remove(storedItem);
            }
        }

    }
}

