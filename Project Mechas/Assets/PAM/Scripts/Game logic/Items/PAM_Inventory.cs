﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class PAM_Inventory : ScriptableObject
{
    public List<PAM_ItemStorage> storage = new List<PAM_ItemStorage>();

    public void AddItem(PAM_Item item, int amount)
    {
        foreach (PAM_ItemStorage storage in storage)
        {
            if (storage.item.name.Equals(item.name))
            {
                storage.amount += amount;
                return;
            }
        }

        storage.Add(new PAM_ItemStorage(item, amount));
    }
}

[System.Serializable]
public class PAM_ItemStorage
{
    public PAM_Item item;
    public int amount;

    public PAM_ItemStorage(PAM_Item item, int amount)
    {
        this.item = item;
        this.amount = amount;
    }

    public int GetWeight()
    {
        return item.weight * amount;
    }
}
