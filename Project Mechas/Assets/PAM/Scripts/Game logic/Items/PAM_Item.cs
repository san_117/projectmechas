﻿using UnityEngine;

[System.Serializable]
public class PAM_Item : ScriptableObject
{
    public Sprite icon;
    public string itemName;
    public ItemType type;
    public int weight;

    public PAM_Item(Sprite icon, string itemName, ItemType type)
    {
        this.icon = icon;
        this.itemName = itemName;
        this.type = type;
    }

    public enum ItemType
    {
        SCRAP, INGOT, ORE, PIECE, PART, MACHINE, MECHA, FUEL
    }
}
