﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class PAM_MinigameJunkClicker : PAM_MinigameManager
{
    public PAM_Garage garage;
    public int garageSlotIndex;
    [SerializeField]
    private PAM_Garage.GarageSlot garageSlot;

    [Space]

    public PAM_ScreenplayReader screenplayReader;
    public Button junk;
    public AudioClip scrap_clip;
    public PAM_CargoInfo cargoInfo;
    public Image[] lootIcon;

    private Rigidbody2D[] _rg;

    public List<PAM_Item> aviableLoot = new List<PAM_Item>();

    private float time = 1;
    private float timer = 1;
    private bool canClick;

    private int indexLoot = 0;

    private void Start()
    {
        garageSlot = garage.storage[garageSlotIndex];

        _rg = new Rigidbody2D[lootIcon.Length];

        for (int i = 0; i < lootIcon.Length - 1 ; i++)
        {
            _rg[i] = lootIcon[i].GetComponent<Rigidbody2D>();
        }
    }

    public void OpenCargoInfo()
    {
        cargoInfo.ShowCargoInfo(garageSlot);
    }

    void Update()
    {
        if (timer < 0)
        {
            timer = time;
            canClick = true;
        }
        else
        {
            timer -= Time.deltaTime;
        }

        for (int i = 0; i < lootIcon.Length - 1; i++)
        {
            if (!lootIcon[i].rectTransform.IsVisibleFrom(Camera.main))
                _rg[i].bodyType = RigidbodyType2D.Static;
        }
    }

    public override void Init()
    {
        junk.interactable = true;
    }

    public void Loot(PAM_Garage.GarageSlot cargoSlot)
    {
        while (!cargoSlot.inventory.IsFull)
        {
            PAM_Item loot = GetRandomLootItem();
            int amount = Random.Range(1, 3);
            cargoSlot.AddItem(loot, amount);
        }
    }

    private PAM_Item GetRandomLootItem()
    {
        return aviableLoot[Random.Range(0, aviableLoot.Count)];
    }

    public void ManualLoot()
    {
        if (!canClick)
            return;

        PAM_Item loot = GetRandomLootItem();
        int amount = Random.Range(1, 3);

        if (garageSlot.inventory.IsFull)
        {
            if (!PAM_GameManager.Singleton.playerData.IsInit)
            {
                screenplayReader.StartScreenplay(screenplayReader.current, 1);
            }
            Debug.Log("Cargo vehicle is full!");
            return;
        }

        garageSlot.AddItem(loot, amount);
        lootIcon[indexLoot].sprite = loot.icon;

        AudioSource.PlayClipAtPoint(scrap_clip, Camera.main.transform.position);
       _rg[indexLoot].bodyType = RigidbodyType2D.Dynamic;

        _rg[indexLoot].velocity = Vector2.zero;
        lootIcon[indexLoot].rectTransform.localPosition = Vector3.zero;
        canClick = false;

        _rg[indexLoot].AddForce(new Vector2(Random.Range(-030, 300), Random.Range(300,500)));

        indexLoot++;

        if(indexLoot >= lootIcon.Length - 1)
        {
            indexLoot = 0;
        }
    }
}
