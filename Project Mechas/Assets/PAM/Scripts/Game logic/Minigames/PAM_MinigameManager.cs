﻿using UnityEngine;
using System.Collections;

public abstract class PAM_MinigameManager : MonoBehaviour
{
    public static void InitGame()
    {
        try
        {
            FindObjectOfType<PAM_MinigameManager>().Init();
        }
        catch
        {
            Debug.Log("Not aviable minigame in scene!");
        }
    }

    public abstract void Init();
}
