﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PAM_Actor: ScriptableObject{

    public string actorName;
    public Sprite sprite_default;
    public Sprite sprite_happy;
    public Sprite sprite_angry;
    public Sprite sprite_scary;
    public Sprite sprite_ashamed;
    public Sprite sprite_sad;
}

[System.Serializable]
public enum ActorParenthetical
{
    DEFAULT, HAPPY, ANGRY, SCARY, ASHAMED, SAD
}