﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PAM_CargoInfo : MonoBehaviour {

    public Text model_text;
    public Text value_text;
    public Text fuel_text;
    public Text age_text;
    public Text cargo_text;

    public Image icon_image;

    public GameObject cargoSlot_prefab;

    public Transform content_list;

    public void ShowCargoInfo(PAM_Garage.GarageSlot slot)
    {
        ClearTable();

        model_text.text = "Model: " + slot.vehicle.model;
        value_text.text = "Value: " + slot.vehicle.value.ToString("F2");
        fuel_text.text = "Fuel: " + slot.vehicle.FuelName;
        age_text.text = "Time used: " + (slot.age / 365).ToString() + " years";
        cargo_text.text = "Cargo: " + slot.inventory.ToString();

        icon_image.sprite = slot.vehicle.icon;

        int index = 0;

        foreach (PAM_ItemStorage storedItem in slot.inventory.storage)
        {
            GameObject prefab = Instantiate(cargoSlot_prefab, content_list, false);
            PAM_CargoInfoSlot cargo_slot = prefab.GetComponent<PAM_CargoInfoSlot>();

            cargo_slot.Init(storedItem, slot, index);

            index++;
        }

        gameObject.SetActive(true);
    }

    private void ClearTable()
    {
        foreach (Transform child in content_list)
        {
            Destroy(child.gameObject);
        }
    }

    private void OnDisable()
    {
        ClearTable();
    }
}
