﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PAM_CargoInfoSlot : MonoBehaviour {

    public Image icon_image;
    public Text name_text;
    public Text amount_text;
    public Image background;

    PAM_ItemStorage currentStoredItem;
    PAM_Garage.GarageSlot currentGarageSlot;

    public void Init(PAM_ItemStorage storedItem, PAM_Garage.GarageSlot garageSlot, int index)
    {
        currentStoredItem = storedItem;
        currentGarageSlot = garageSlot;

        icon_image.sprite = storedItem.item.icon;
        name_text.text = storedItem.item.itemName;
        amount_text.text = storedItem.amount + "u / " + storedItem.amount * storedItem.item.weight + "kg";

        Color background = Color.white;

        if(index % 2 != 0)
        {
            background.a = 0.1f;
        }
        else{
            background.a = 0.2f;
        }

        this.background.color = background;
    }

    public void Drop()
    {
        currentGarageSlot.inventory.RemoveItem(currentStoredItem);
        GetComponentInParent<PAM_CargoInfo>().ShowCargoInfo(currentGarageSlot);
        Destroy(this.gameObject);
    }
}
