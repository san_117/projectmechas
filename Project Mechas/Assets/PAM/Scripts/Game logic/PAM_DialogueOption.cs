﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PAM_DialogueOption : MonoBehaviour
{
    public delegate void Activation(PAM_SpeachActionData data);
    public Activation Action;

    public PAM_OptionManager optionManager;
    public CanvasGroup group;
    public Text text_area;

    public void SelectOption()
    {
        StartCoroutine(optionManager.HideOptionsAimated(this));
    }

    public void Hide()
    {
        group.alpha = 0;
        gameObject.SetActive(false);
    }

    public IEnumerator Show(PAM_SpeachActionOption option)
    {
        gameObject.SetActive(true);
        text_area.text = option.message;
        Action = option.Activate;

        while (group.alpha < 1)
        {
            group.alpha += Time.deltaTime * 5;
            yield return new WaitForSeconds(0.01f);
        }
    }

    public IEnumerator HideAnimated()
    {
        while (group.alpha > 0)
        {
            group.alpha -= Time.deltaTime * 5;
            yield return new WaitForSeconds(0.01f);
        }
    }
}
