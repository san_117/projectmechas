﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PAM_DialogueWriter : MonoBehaviour
{
    public Animator anim;
    public AudioClip soundEffect;
    public Text text;
    public Text actorName;
    public Image arrow;

    private Font defaultFont;

    private bool stop;
    PAM_SpeachActionData currentActionData;

    public bool IsWriting { get; private set; }

    private void Start()
    {
        defaultFont = text.font;
    }

    void Update()
    {
        if (IsWriting)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                stop = true;
            }
        }

        if(currentActionData != null)
        {
            if (currentActionData.isCompleted)
            {
                arrow.enabled = false;
                text.text = string.Empty;
                actorName.text = string.Empty;
                currentActionData = null; 
            }
        }
    }

    public IEnumerator ShowDialogue(PAM_SpeachActionData actionData)
    {
        float speed = (1- PAM_GameManager.Singleton.settingsData.DialogueSpeed) * Time.deltaTime * 3;
        int i = 0;
        string str = "";
        string currentMessage = actionData.dialogue;

        text.font = defaultFont;

        if (actionData.font != null)
            text.font = actionData.font;

        currentActionData = actionData;
        stop = false;
        IsWriting = true;
        text.text = string.Empty;
        arrow.enabled = false;

        if (!anim.GetBool("IsOpen"))
        {
            yield return StartCoroutine(OpenDialoguePanel());
        }

        actorName.text = actionData.actorName;

        while (i < currentMessage.Length)
        {
            if (stop)
                break;

            str += currentMessage[i++];
            text.text = str;
            AudioSource.PlayClipAtPoint(soundEffect, Camera.main.transform.position, PAM_GameManager.Singleton.settingsData.SFX_Volume);
            yield return new WaitForSeconds(speed);
        }

        text.text = currentMessage;
        arrow.enabled = true;
        IsWriting = false;
    }

    private IEnumerator OpenDialoguePanel()
    {
        text.text = string.Empty;
        actorName.text = string.Empty;

        anim.SetBool("IsOpen", true);

        yield return new WaitForSeconds(1);
    }

    public IEnumerator CloseDialoguePanel()
    {
        text.text = string.Empty;
        actorName.text = string.Empty;

        anim.SetBool("IsOpen", false);

        yield return new WaitForSeconds(1);
    }
}
