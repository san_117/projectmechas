﻿using UnityEngine;
using System.Collections;

public class PAM_MusicManager : MonoBehaviour
{
    public AudioSource musicSource;

    void Start()
    {
        musicSource.volume = PAM_GameManager.Singleton.settingsData.Music_Volume;
    }

    public void ChangeMusic(AudioClip newMusic)
    {
        musicSource.clip = newMusic;
        musicSource.Play();
    }
}
