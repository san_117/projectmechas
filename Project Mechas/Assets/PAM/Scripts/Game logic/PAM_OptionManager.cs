﻿using UnityEngine;
using System.Collections;

public class PAM_OptionManager : MonoBehaviour
{
    public PAM_DialogueOption[] buttons;
    public Material BlurUI;

    private PAM_SpeachActionData data;

    private void Start()
    {
        BlurUI.SetFloat("_Size", 0);
        HideOptions();
    }

    private void HideOptions()
    {
        foreach (PAM_DialogueOption option in buttons)
        {
            option.Hide();
        }
    }

    public IEnumerator HideOptionsAimated(PAM_DialogueOption selected)
    {
        foreach (PAM_DialogueOption option in buttons)
        {
            if (!option.Equals(selected))
            {
                yield return StartCoroutine(option.HideAnimated());
            }
        }

        yield return new WaitForSeconds(0.3f);

        yield return StartCoroutine(selected.HideAnimated());

        yield return new WaitForSeconds(0.3f);

        selected.Action(data);

        HideOptions();
    }

    private IEnumerator Blur(BlurMode mode)
    {
        if (mode.Equals(BlurMode.BLUR_IN))
        {
            float amount = BlurUI.GetFloat("_Size");

            while (amount < 0.95f)
            {
                amount += Time.deltaTime * 5;
                BlurUI.SetFloat("_Size", amount);
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            float amount = BlurUI.GetFloat("_Size");

            while (amount > 0.1f)
            {
                amount -= Time.deltaTime * 5;
                BlurUI.SetFloat("_Size", amount);
                yield return new WaitForEndOfFrame();
            }
        }
    }

    public IEnumerator ShowOptions(PAM_SpeachActionData speach)
    {
        data = speach;

        yield return StartCoroutine(Blur(BlurMode.BLUR_IN));

        for (int i = 0; i < speach.options.Count; i++)
        {
            buttons[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < speach.options.Count; i++)
        {
            yield return StartCoroutine(buttons[i].Show(speach.options[i]));
        }

        while (!speach.isCompleted)
        {
            yield return new WaitForEndOfFrame();
        }

        yield return StartCoroutine(Blur(BlurMode.BLUR_OUT));
    }

    private enum BlurMode
    {
        BLUR_IN, BLUR_OUT
    }
}
