﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PAM_ScenarioManager : MonoBehaviour
{
    public Image background;
    private float speed = 0.1f;

    public IEnumerator ChangeScenario(Sprite newScenario)
    {
        yield return Fade(FadeMode.FADE_IN);

        background.sprite = newScenario;

        yield return Fade(FadeMode.FADE_OUT);
    }

    private IEnumerator Fade(FadeMode mode)
    {
        if (mode.Equals(FadeMode.FADE_IN))
        {
            while (background.color.r > 0.1f)
            {
                background.color = Color.Lerp(background.color, Color.black, Time.deltaTime);
                yield return new WaitForSeconds(speed * Time.deltaTime);
            }

            background.color = Color.black;
        }
        else
        {
            while (background.color.r < 0.9f)
            {
                background.color = Color.Lerp(background.color, Color.white, Time.deltaTime);
                yield return new WaitForSeconds(speed * Time.deltaTime);
            }

            background.color = Color.white;
        }
    }

    private enum FadeMode
    {
        FADE_IN, FADE_OUT
    }
}
