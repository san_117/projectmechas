﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class PAM_Screenplay : ScriptableObject
{
    public PAM_Scenario scenario;
    public List<PAM_ActorSpeach> actorsSpeachs = new List<PAM_ActorSpeach> ();
}

[System.Serializable]
public class PAM_ActorSpeach
{
    public PAM_Actor actor;
    public List<PAM_SpeachActionData> actions = new List<PAM_SpeachActionData>();
}
