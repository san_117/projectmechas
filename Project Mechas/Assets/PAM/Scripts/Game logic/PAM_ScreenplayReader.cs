﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PAM_ScreenplayReader : MonoBehaviour {

    public PAM_Screenplay current;
    public PAM_DialogueWriter dialogueWriter;
    public PAM_MusicManager musicManager;
    public PAM_ScenarioManager scenarioManager;
    public PAM_OptionManager optionManager;
    public Animator Backdrop;
    public Image background;

    private static PAM_ScreenplayReader _singleton;

    public static PAM_ScreenplayReader Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = FindObjectOfType<PAM_ScreenplayReader>();

            return _singleton;
        }
    }

    IEnumerator Start()
    {
        background.sprite = current.scenario.background;

        yield return new WaitForSeconds(2);

        StartScreenplay(current);
    }

    IEnumerator ReadingScreenplay()
    {
        background.sprite = current.scenario.background; 

        foreach (PAM_ActorSpeach speach in current.actorsSpeachs)
        {
            foreach (PAM_SpeachActionData action in speach.actions)
            {
                if (action.isCompleted)
                    continue;

                while (!action.isCompleted)
                {
                    switch (action.Type)
                    {
                        case SpeachAction.PARENTHETICAL:
                            break;
                        case SpeachAction.DIALOGUE:

                            yield return StartCoroutine(dialogueWriter.ShowDialogue(action));

                            yield return new WaitForSeconds(0.5f);

                            yield return StartCoroutine(WaitForKey(KeyCode.Space));

                            action.isCompleted = true;
                            break;
                        case SpeachAction.OPTIONS:
                            yield return StartCoroutine(optionManager.ShowOptions(action));
                            break;
                        case SpeachAction.SFX:
                            AudioSource.PlayClipAtPoint(action.sfx, Camera.main.transform.position, PAM_GameManager.Singleton.settingsData.SFX_Volume);
                            action.isCompleted = true;
                            break;
                        case SpeachAction.MUSIC:
                            musicManager.ChangeMusic(action.music);
                            action.isCompleted = true;
                            break;
                        case SpeachAction.BACKGROUND:
                            yield return StartCoroutine(scenarioManager.ChangeScenario(action.background));
                            action.isCompleted = true;
                            break;
                        case SpeachAction.WAITFORSECONDS:
                            yield return new WaitForSeconds(action.delay);
                            action.isCompleted = true;
                            break;
                        case SpeachAction.OPEN_SCREENPLAY:
                            action.isCompleted = true;
                            StartScreenplay(action.nextScreenplay, action.initLine);
                            break;
                        case SpeachAction.LOAD_SCENE:
                            StartCoroutine(FinishScene(action.nextScene));
                            break;
                        case SpeachAction.INIT_GAME:
                            InitGame();
                            break;
                    }

                    yield return new WaitForEndOfFrame();
                }
            }
        }

        yield return null;
    }

    IEnumerator WaitForKey(KeyCode keyCode)
    {
        while (!Input.GetKey(keyCode))
            yield return null;
    }

    public void FinishScreenplay(string nextScene)
    {
        StopAllCoroutines();

        _singleton = null;

        UnityEngine.SceneManagement.SceneManager.LoadScene(nextScene);
    }

    public void SwitchScreenplay()
    {
        StartCoroutine(ReadingScreenplay());
    }

    public void InitGame()
    {
        StartCoroutine(dialogueWriter.CloseDialoguePanel());

        PAM_MinigameManager.InitGame();
    }

    public IEnumerator FinishScene(string nextScene)
    {
        yield return StartCoroutine(dialogueWriter.CloseDialoguePanel());

        Backdrop.SetTrigger("FinishScene");

        FinishScreenplay(nextScene);
    }

    public void StartScreenplay(PAM_Screenplay screenplay)
    {
        StopAllCoroutines();

        current = screenplay;

        foreach (PAM_ActorSpeach actorSpeach in current.actorsSpeachs)
        {
            foreach (PAM_SpeachActionData data in actorSpeach.actions)
            {
                data.isCompleted = false;
            }
        }

        SwitchScreenplay();
    }

    public void StartScreenplay(PAM_Screenplay screenplay, int line)
    {
        StopAllCoroutines();

        current = screenplay;

        foreach (PAM_ActorSpeach actorSpeach in current.actorsSpeachs)
        {
            foreach (PAM_SpeachActionData data in actorSpeach.actions)
            {
                data.isCompleted = false;
            }
        }

        for (int i = 0; i < line; i++)
        {
            foreach (PAM_SpeachActionData data in current.actorsSpeachs[i].actions)
            {
                data.isCompleted = true;
            }
        }

        SwitchScreenplay();
    }
}
