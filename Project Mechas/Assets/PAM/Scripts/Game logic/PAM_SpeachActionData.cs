﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class PAM_SpeachActionData
{
    [SerializeField]
    private SpeachAction _type;

    public bool isCompleted;

    //Parenthetical
    public ActorParenthetical changeExpressionTo;

    //Dialogue
    public string dialogue;
    public string actorName;
    public Font font;

    //Option
    public List<PAM_SpeachActionOption> options = new List<PAM_SpeachActionOption>();

    //SFX
    public AudioClip sfx;

    //Music
    public AudioClip music;

    //Background
    public Sprite background;

    //Delay
    public float delay;

    //Screenplay
    public PAM_Screenplay nextScreenplay;
    public int initLine;

    //Scene
    public string nextScene;

    public SpeachAction Type
    {
        get
        {
            return _type;
        }
    }

    public PAM_SpeachActionData(SpeachAction type)
    {
        this._type = type;
    }
}

[System.Serializable]
public enum SpeachAction
{
    PARENTHETICAL,
    DIALOGUE,
    OPTIONS,
    SFX,
    MUSIC,
    BACKGROUND,
    WAITFORSECONDS,
    OPEN_SCREENPLAY,
    LOAD_SCENE,
    INIT_GAME
}
