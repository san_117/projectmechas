﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PAM_SpeachActionOption
{
    public string message;

    public PAM_Screenplay nextScreenPlay;
    public int initLine;

    [SerializeField]
    private ActionOption _optionType = ActionOption.SELECT_ONE;

    public ActionOption OptionType
    {
        get
        {
            return _optionType;
        }

        set
        {
            _optionType = value;
        }
    }

    public void Activate(PAM_SpeachActionData data)
    {
        switch (_optionType)
        {
            case ActionOption.SELECT_ONE:
                Debug.Log("You must select one option!");
                break;
            case ActionOption.CONTINUE_SPEACH:
                ContinueSpeach(data);
                break;
            case ActionOption.START_SCREENPLAY:
                StartScreenplay(data);
                break;
            case ActionOption.CONTINUE_SCREENPLAY:
                ContinueScreenplay(data);
                break;
        }

    }

    private void ContinueSpeach(PAM_SpeachActionData data)
    {
        data.isCompleted = true;
    }

    private void StartScreenplay(PAM_SpeachActionData data)
    {
        data.isCompleted = true;

        PAM_ScreenplayReader.Singleton.StartScreenplay(nextScreenPlay);
    }

    private void ContinueScreenplay(PAM_SpeachActionData data)
    {
        data.isCompleted = true;
        PAM_ScreenplayReader.Singleton.StartScreenplay(nextScreenPlay, initLine);
    }
}

[System.Serializable]
public enum ActionOption
{
    SELECT_ONE,
    CONTINUE_SPEACH,
    START_SCREENPLAY,
    CONTINUE_SCREENPLAY,
}
