﻿using System;
using UnityEngine;

[System.Serializable]
public abstract class PAM_Vehicle : ScriptableObject
{
    public string model;
    public float value;
    public Sprite icon;
    [SerializeField]
    private PAM_Fuel fuel;
    [SerializeField]
    private VehicleType vehicleType;

    public VehicleType GetVehicleType()
    {
        return vehicleType;
    }

    public string FuelName
    {
        get
        {
            return fuel.itemName;
        }
    }

    public PAM_Vehicle(string model, float value, Sprite icon, PAM_Fuel fuel, VehicleType vehicleType)
    {
        this.model = model;
        this.value = value;
        this.icon = icon;
        this.fuel = fuel;
        this.vehicleType = vehicleType;
    }

    public enum VehicleType
    {
        TRANSPORT, CARGO, MECHA
    }
}
