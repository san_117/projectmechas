﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PAM_VehicleCargo : PAM_Vehicle
{
    [SerializeField]
    private int maxCapacity;

    public int MaxCapacity
    {
        get
        {
            return maxCapacity;
        }
    }

    public PAM_VehicleCargo(string model, float value, Sprite icon, PAM_Fuel fuel, VehicleType vehicleType, int capacity) : base(model, value, icon, fuel, vehicleType)
    {
        this.maxCapacity = capacity;
    }
}