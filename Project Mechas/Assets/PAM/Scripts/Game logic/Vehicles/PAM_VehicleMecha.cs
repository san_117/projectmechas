﻿using UnityEngine;

[System.Serializable]
public class PAM_VehicleMecha : PAM_Vehicle
{
    public MechaType mechaType;

    public PAM_VehicleMecha(string model, float value, Sprite icon, PAM_Fuel fuel, VehicleType vehicleType, MechaType mechaType) : base(model, value, icon, fuel, vehicleType)
    {
        this.mechaType = mechaType;
    }

    public enum MechaType
    {
        DEFENSE, ATTACK, HELPER, EXPLORATION
    }
}