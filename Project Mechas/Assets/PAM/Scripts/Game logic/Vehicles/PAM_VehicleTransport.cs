﻿using UnityEngine;

[System.Serializable]
public class PAM_VehicleTransport : PAM_Vehicle
{
    public PAM_VehicleTransport(string model, float value, Sprite icon, PAM_Fuel fuel, VehicleType vehicleType) : base(model, value, icon, fuel, vehicleType)
    {
    }
}
