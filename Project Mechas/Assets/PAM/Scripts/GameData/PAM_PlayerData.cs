﻿using UnityEngine;
using System.Collections;

public class PAM_PlayerData: ScriptableObject
{
    [SerializeField]
    private bool isInit = false;

    public GamePhase phase = GamePhase.PROLOGUE;

    public void ResetData()
    {
        isInit = false;
        phase = GamePhase.PROLOGUE;
    }

    public bool IsInit
    {
        get
        {
            return isInit;
        }
    }

    public string LastScene
    {
        get
        {
            switch (phase)
            {
                case GamePhase.PROLOGUE:
                    return "PAM_Prologue";
                case GamePhase.BASE_GAME:
                    return "";
                case GamePhase.FIRST_PROBLEMS:
                    return "";
                case GamePhase.INTERMEDIATE_GAME:
                    return "";
                case GamePhase.PORTAL:
                    return "";
                case GamePhase.ADVANCED_GAME:
                    return "";
                default:
                    return "";
            }
        }
    }

    public enum GamePhase
    {
        PROLOGUE, BASE_GAME, FIRST_PROBLEMS, INTERMEDIATE_GAME, PORTAL, ADVANCED_GAME
    }
}
