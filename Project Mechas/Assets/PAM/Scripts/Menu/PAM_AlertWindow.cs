﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PAM_AlertWindow : MonoBehaviour {

    public Text text;

    public bool IsOver { get; private set; }
    public bool Result { get; private set; }

    public void Open(string message)
    {
        IsOver = false;
        this.text.text = message;

        gameObject.SetActive(true);
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.Y))
        {
            Result = true;
            IsOver = true;
            gameObject.SetActive(false);
        }

        if (Input.GetKey(KeyCode.N))
        {
            Result = false;
            IsOver = true;
            gameObject.SetActive(false);
        }
    }
}
