﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PAM_AudioConfig : MonoBehaviour
{
    public Text sfx_bar_text;
    public Text music_bar_text;

    public void OnEnable()
    {
        UpdateBars();
    }

    private void UpdateBars()
    {
        int bar = (int)(PAM_GameManager.Singleton.settingsData.SFX_Volume * 10);

        sfx_bar_text.text = string.Empty;

        for (int i = 0; i < bar; i++)
        {
            sfx_bar_text.text += "|";
        }

        bar = (int)(PAM_GameManager.Singleton.settingsData.Music_Volume * 10);

        music_bar_text.text = string.Empty;

        for (int i = 0; i < bar; i++)
        {
            music_bar_text.text += "|";
        }
    }

    public void AddVolumeSFX()
    {
        PAM_GameManager.Singleton.settingsData.SFX_Volume += 0.1f;
        PAM_GameManager.UpdateAudioSources();
        UpdateBars();

        AudioSource.PlayClipAtPoint(PAM_Resources.SFX_Move_Menu, Camera.main.transform.position, PAM_GameManager.Singleton.settingsData.SFX_Volume);
    }

    public void ReduceVolumeSFX()
    {
        PAM_GameManager.Singleton.settingsData.SFX_Volume -= 0.1f;
        PAM_GameManager.UpdateAudioSources();
        UpdateBars();

        AudioSource.PlayClipAtPoint(PAM_Resources.SFX_Move_Menu, Camera.main.transform.position, PAM_GameManager.Singleton.settingsData.SFX_Volume);
    }

    public void AddVolumeMusic()
    {
        PAM_GameManager.Singleton.settingsData.Music_Volume += 0.1f;
        PAM_GameManager.UpdateAudioSources();
        UpdateBars();
    }

    public void ReduceVolumeMusic()
    {
        PAM_GameManager.Singleton.settingsData.Music_Volume -= 0.1f;
        PAM_GameManager.UpdateAudioSources();
        UpdateBars();
    }
}
