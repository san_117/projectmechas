﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PAM_ButtonMenu : Button {

    private string label;
    private Text _component;

    private Text Component
    {
        get
        {
            if (_component == null)
            {
                _component = transform.GetComponent<Text>();
            }

            return _component;
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {

    }

    public override void OnPointerExit(PointerEventData eventData)
    {

    }

    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);
        label = Component.text;

        Component.text += "  <---";

        AudioSource.PlayClipAtPoint(PAM_Resources.SFX_Move_Menu, Camera.main.transform.position, PAM_GameManager.Singleton.settingsData.SFX_Volume);
    }

    public override void OnDeselect(BaseEventData eventData)
    {
        base.OnDeselect(eventData);
        Component.text = label;
    }
}
