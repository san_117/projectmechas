﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PAM_Console : MonoBehaviour {

    public Text console;

    public void Write(string command)
    {
        StartCoroutine(Tiping(command));
    }

    IEnumerator Tiping(string text)
    {
        int i = 0;
        string str = "C:\\";
        
        while (i < text.Length)
        {
            str += text[i++];
            console.text = str;
            yield return new WaitForSeconds(Time.deltaTime * 0.2f);
        }
    }
	
}
