﻿using UnityEngine;
using UnityEngine.PostProcessing;
using System.Collections;

public class PAM_GameManager : MonoBehaviour
{
    public PAM_PlayerData playerData;
    public PAM_Settings settingsData;
    private static PAM_GameManager _singleton;
    private bool isInit;

    public static PAM_GameManager Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = FindObjectOfType<PAM_GameManager>();

            return _singleton;
        }
    }

    private void Start()
    {
        if (!Singleton.isInit)
        {
            DontDestroyOnLoad(gameObject);
            Singleton.isInit = true;
            UpdateAudioSources();
            UpdateEffects();
        }
    }

   public void InitGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(playerData.LastScene);
    }

    public static void UpdateAudioSources()
    {
         foreach (AudioSource item in Object.FindObjectsOfType<AudioSource>())
        {
            item.volume = PAM_GameManager.Singleton.settingsData.Music_Volume;
        }
    }

    public static void UpdateEffects()
    {
        foreach (PostProcessingBehaviour item in Object.FindObjectsOfType<PostProcessingBehaviour>())
        {
            item.enabled = PAM_GameManager.Singleton.settingsData.Effects;
        }
    }
}
