﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PAM_GameplayConfig : MonoBehaviour
{
    public Text speed_bar_text;

    public void OnEnable()
    {
        UpdateBars();
    }

    private void UpdateBars()
    {
        int bar = (int)(PAM_GameManager.Singleton.settingsData.DialogueSpeed * 10 );

        speed_bar_text.text = string.Empty;

        for (int i = 0; i < bar; i++)
        {
            speed_bar_text.text += "|";
        }
    }

    public void AddDialogueSpeed()
    {
        PAM_GameManager.Singleton.settingsData.DialogueSpeed += 0.1f;
        UpdateBars();
    }

    public void ReduceDialogueSpeed()
    {
        PAM_GameManager.Singleton.settingsData.DialogueSpeed -= 0.1f;
        UpdateBars();
    }
}
