﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PAM_MainMenu : MonoBehaviour
{
    public Button continue_button;
    public PAM_AlertWindow alertWindow;

    private delegate void ButtonAction();
    private ButtonAction ButtonActionDelegate;

    void Start()
    {
        continue_button.interactable = PAM_GameManager.Singleton.playerData.IsInit;
    }

    public void Continue()
    {
        PAM_GameManager.Singleton.InitGame();
    }

    public void NewGame()
    {
        if(PAM_GameManager.Singleton.playerData.IsInit)
        {
            alertWindow.Open("Will you lose the previous progress, are you sure to continue (Y/N)?");

            ButtonActionDelegate = CreateNewGame;

            StartCoroutine(WaitForAlertWindow());
        }
        else
        {
            PAM_GameManager.Singleton.InitGame();
        }
    }

    public void More()
    {
        alertWindow.Open("You will be redirected to an external link, do you want to continue (Y/N)?");

        ButtonActionDelegate = OpenLink;

        StartCoroutine(WaitForAlertWindow());
    }

    public void Quit()
    {
        alertWindow.Open("You are sure you want to close the game (Y/N)?");

        ButtonActionDelegate = ExitGame;

        StartCoroutine(WaitForAlertWindow());
    }

    private void OpenLink()
    {
        Application.OpenURL("https://www.facebook.com/Random-Adjective-371110100050357/?modal=admin_todo_tour");
    }

    private void ExitGame()
    {
        Application.Quit();
    }

    private void CreateNewGame()
    {
        PAM_GameManager.Singleton.playerData.ResetData();
        PAM_GameManager.Singleton.InitGame();
    }

    IEnumerator WaitForAlertWindow()
    {
        while (!alertWindow.IsOver)
        {
            yield return new WaitForSeconds(Time.deltaTime);
        }

        if (alertWindow.Result)
            ButtonActionDelegate();
    }
}
