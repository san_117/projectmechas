﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PAM_PanelMenu : MonoBehaviour {

    private Text[] textsInPanel;
    private string[] savedTexts;

    private static void CloseAllPanels()
    {
        foreach (PAM_PanelMenu panel in FindObjectsOfType<PAM_PanelMenu>())
        {
            panel.gameObject.SetActive(false);
        }
    }

	public void OpenPanel()
    {
        CloseAllPanels();

        textsInPanel = null;

        gameObject.SetActive(true);

        textsInPanel = transform.GetComponentsInChildren<Text>();
        ShuffleList();

        StartCoroutine(SelectingTexts());
    }

    private void ShuffleList()
    {
        savedTexts = new string[textsInPanel.Length];

        for (int i = 0; i < textsInPanel.Length; i++)
        {
            Text tempText = textsInPanel[i];
            int rnd = Random.Range(i, textsInPanel.Length);
            textsInPanel[i] = textsInPanel[rnd];
            textsInPanel[rnd] = tempText;

            savedTexts[i] = textsInPanel[i].text;
            textsInPanel[i].text = string.Empty;
        }
    }

    private IEnumerator SelectingTexts()
    {
        for (int i = 0; i < textsInPanel.Length; i++)
        {
            float aviableTime = Random.Range(0.01f, 0.1f);
            StartCoroutine(DisplayText(aviableTime, i));
            yield return new WaitForSeconds(aviableTime);
        }
    }

    private IEnumerator DisplayText(float aviableTime, int index)
    {
        int i = 0;
        string str = "";
        string strComplete = savedTexts[index];

        float totalTime = (aviableTime / strComplete.Length) * PAM_GameManager.Singleton.settingsData.DialogueSpeed * 10;

        while (i < strComplete.Length)
        {
            str += strComplete[i++];
            textsInPanel[index].text = str;
            yield return new WaitForSeconds(totalTime);
        }
    }
}
