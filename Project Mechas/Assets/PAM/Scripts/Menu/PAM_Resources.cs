﻿using UnityEngine;
using System.Collections;

public static class PAM_Resources
{
    private static AudioClip _sfx_sound;

    public static AudioClip SFX_Move_Menu
    {
        get
        {
            if (_sfx_sound == null)
                _sfx_sound = Resources.Load<AudioClip>("PAM/Audio/SFX/Tap");

            return _sfx_sound;
        }
    }
}
