﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[System.Serializable]
public class PAM_Settings: ScriptableObject
{
    public float sfx_volume = 1;
    public float music_volume = 1;
    public float dialogues_speed = 1;
    public bool effects = false;

    [MenuItem("PAM/Settings/Create")]
    private static void Create()
    {
        AssetDatabase.CreateAsset(new PAM_Settings(), "Assets/PAM/Screenplay/Player Data/Settings.asset");
    }

    public float DialogueSpeed
    {
        get
        {
            return dialogues_speed;
        }

        set
        {
            dialogues_speed = value;
            dialogues_speed = Mathf.Clamp(dialogues_speed, 0.01f, 1f);
        }
    }

    public bool Effects
    {
        get
        {
            return effects;
        }

        set
        {
            effects = value;
        }
    }

    public float SFX_Volume
    {
        get
        {
            return sfx_volume;
        }

        set
        {
            sfx_volume = value;
            sfx_volume = Mathf.Clamp(sfx_volume, 0f, 1f);
        }
    }

    public float Music_Volume
    {
        get
        {
            return music_volume;
        }

        set
        {
            music_volume = value;
            music_volume = Mathf.Clamp(music_volume, 0f, 1f);
        }
    }

    
}
