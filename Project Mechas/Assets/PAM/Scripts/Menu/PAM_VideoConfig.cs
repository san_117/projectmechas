﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PAM_VideoConfig : MonoBehaviour
{
    public Toggle effects_toggle;

    public void ActiveEffect()
    {
        PAM_GameManager.Singleton.settingsData.Effects = effects_toggle.isOn;
        PAM_GameManager.UpdateEffects();
    }
}
